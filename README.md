# Mallory: A Docker Container for MITM Experiments

This repository contains a Docker container for running MITM experiments. The
container creates a Wi-Fi network where connections established by clients to
ports 80 (HTTP) and 443 (HTTPS) are transparently redirected to an instance of
[mitmproxy](https://mitmproxy.org). The proxy is configured to dump TLS session
keys to a file. The container also runs tshark which creates a pcap dump file
with all the traffic in the Wi-Fi network. The pcap dump file together with the
TLS session key file can be used to decrypt TLS connections 
in [Wireshark](https://wireshark.org).

Use the script `run` to start the container in interactive mode. The script
expects the name of the Wi-Fi interface as the first argument. The interface
will be moved to the namespace of the container once the container has been
started. The container configures the interface in AP mode and creates a
network with SSID 'Mallory' and a WPA/WPA2 password 'mallory123'. Once the
container is running, a `tmux` session is created with `hostapd`, `dnsmasq`, 
`mitmproxy`, and `thark` in individual windows.

The MITM proxy dumps TLS session keys to file `/data/sslkeys.txt`. Pcap dumps
of Wi-Fi traffic are saved to `/data/traffic.pcapng`. Mitmproxy's CA
certificate will be saved in `/data/mitmproxy-ca-cert.pem`. The CA certificate
does not change across container restarts.

The script `run` mounts the `/data` directory in the container to a directory
on the host. It creates a subdirectory in the format `data/YYMMDD-HHMMSS` in
the directory of the script, where YYMMDD-HHMMSS will be replaced with current
date and time. This naming strategy ensures that the dump files produced during
different runs of the container will not overwrite each other.

Note: In order for the MITM attack to be successfull, you must configure TLS
clients to trust Mitmproxy's CA certificate.
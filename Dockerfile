FROM ubuntu
MAINTAINER Jan Janak <jan@janakj.org>

RUN apt-get update                                                   \
 && apt-get -y install --no-install-recommends --no-install-suggests \
      hostapd                                                        \
      iptables                                                       \
      iproute2                                                       \
      net-tools                                                      \
      tcpdump                                                        \
      nmap                                                           \
      telnet                                                         \
      tmux                                                           \
      locales                                                        \
      dnsmasq                                                        \
      python3                                                        \
      tshark                                                         \
      python3-venv

RUN python3 -m venv /mitmproxy \
 && /mitmproxy/bin/pip install mitmproxy

RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment \
 && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen   \
 && echo "LANG=en_US.UTF-8" > /etc/locale.confa   \
 && locale-gen en_US.UTF-8
ENV LANG US.UTF-8

RUN mkdir /data
ADD hostapd.conf /etc/hostapd
ADD mallory.sh   /usr/local/bin
ADD dnsmasq.conf /etc
ADD .mitmproxy/  /root/.mitmproxy

ENTRYPOINT ["mallory.sh"]


#!/bin/bash

ifname=$1; shift

echo "Interface: $ifname"

echo "Enabling IP forwarding"
echo "1" > /proc/sys/net/ipv4/ip_forward

echo "Setting up NAT"
iptables -t nat -A POSTROUTING -s 192.168.0/24 -j MASQUERADE

echo "Redirecting port 80 to mitmproxy"
iptables -t nat -A PREROUTING -i $ifname -p tcp --dport 80 -j REDIRECT --to-port 8080
echo "Redirecting port 443 to mitmproxy"
iptables -t nat -A PREROUTING -i $ifname -p tcp --dport 443 -j REDIRECT --to-port 8080

cp /root/.mitmproxy/mitmproxy-ca-cert* /data

tmux new-session \;                                    \
  new-window -n 'hostapd' \;                           \
  send-keys "while true ; do                           \
    ip addr add 192.168.0.1/24 dev $ifname             \
      && hostapd -i $ifname /etc/hostapd/hostapd.conf ;\
    sleep 1 ;                                          \
  done" C-m \;                                         \
                                                       \
  new-window -n 'dnsmasq' \;                           \
  send-keys "dnsmasq -d -C /etc/dnsmasq.conf" C-m \;   \
                                                       \
  new-window -n 'mitmproxy' \;                         \
  send-keys "SSLKEYLOGFILE=/data/sslkeys.txt /mitmproxy/bin/mitmproxy --mode transparent --showhost --ssl-insecure --rawtcp" C-m \; \
                                                       \
  new-window -n 'tshark' \;                            \
  send-keys "touch /data/traffic.pcapng && tshark -P -i any -w /data/traffic.pcapng" C-m

